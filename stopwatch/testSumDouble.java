package stopwatch;
/**
 * a testSumDouble class.
 * @author Narut Poovorakit
 * @version 31.01.2015
 */
public class testSumDouble implements Runnable {
	//create instance variables.
	private int counter;
	private int ARRAY_SIZE;
	private Double sum;

	/**
	 * initiate a constructor.
	 * @param counter that bring from speedtest.
	 * @param ARRAYSIZE is a size of an array.
	 */
	public testSumDouble(int counter, int ARRAYSIZE) {
		this.counter = counter;
		this.ARRAY_SIZE = ARRAYSIZE;
	}

	//a run method.
	public void run() {
		Double[] values = new Double[ARRAY_SIZE];
		for (int i = 0; i < ARRAY_SIZE; i++)
			values[i] = new Double(i + 1);

		sum = new Double(0.0);
		// count = loop counter, i = array index
		for (int count = 0, i = 0; count < counter; count++, i++) {
			if (i >= ARRAY_SIZE)
				i = 0;
			sum = sum + values[i];
		}
	}

	//print a toString.
	public String toString() {
		return String.format("Sum array of Double objects with count=%,d\n",
				counter);
	}

	//print a toString final
	public String toStringFinal() {
		return "sum = " + sum + "\n";
	}

	//print an elapsed time
	public String toStringElapsed(StopWatch timer) {
		return String.format("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}

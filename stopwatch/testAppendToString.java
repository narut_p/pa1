package stopwatch;
/**
 * A testAppendToString class.
 * @author Narut Poovorakit
 * @version 31.01.2015
 *
 */
public class testAppendToString implements Runnable{
	//create instance variables counter,sum and CHAR.
	private int counter;
	String sum = "";
	final char CHAR = 'a';

	/**
	 * initiate the constructor.
	 * @param counter that bring from speedtest class.
	 */
	public testAppendToString(int counter) {

		this.counter = counter;
	}

	//run the method.
	public void run() {
		int k = 0;
		while (k++ < counter) {
			sum = sum + CHAR;
		}

	}
	
	//print toString.
	public String toString() {
		return String.format("Append to String with count=%,d\n", counter);
	}
	//print toString final.
	public String toStringFinal() {
		return String.format("final string length = " + sum.length() + "\n");
	}
	//print toStringElapsed.
	public String toStringElapsed(StopWatch timer) {
		return String.format("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}

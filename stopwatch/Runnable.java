package stopwatch;

/**
 * 
 * @author Narut Poovorakit
 *@version 15.01.31
 */
public interface Runnable {
	//a method run
	public void run();
	
	//a method that print String final.
	public String toStringFinal();

	//a method that print String elapsed.
	public String toStringElapsed(StopWatch timer);

	//a method that print toString.
	public String toString();

}

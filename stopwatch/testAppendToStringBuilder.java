package stopwatch;
/**
 * A testAppendToStringBuilder class.
 * @author Narut Poovorakit
 * @version 31.01.2015
 *
 */
public class testAppendToStringBuilder implements Runnable {
	//create instance variables.
	private int counter;
	final char CHAR = 'a';
	String result;

	//initiate the constructor.
	public testAppendToStringBuilder(int counter) {
		this.counter = counter;
	}

	//run the method.
	public void run() {
		StringBuilder builder = new StringBuilder();
		int k = 0;
		while (k++ < counter) {
			builder = builder.append(CHAR);

		}
		result = builder.toString();
	}

	//get a result
	public String getResult() {
		return result;
	}

	//print toString.
	public String toString() {
		return String.format("Append to StringBuilder with count=%,d\n",
				counter);
	}

	//print toStringFinal.
	public String toStringFinal() {
		return "final string length = " + getResult().length() + "\n";
	}

	//print toStringElapsed.
	public String toStringElapsed(StopWatch timer) {
		return String.format("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}

package stopwatch;
/**
 * a testSumDoublePrimitive class.
 * @author Narut Poovorakit
 * @version 31.01.2015
 *
 */
public class testSumDoublePrimitive implements Runnable {
	//create an instance variables.
	private int counter;
	private int ARRAY_SIZE;
	private double sum;

	//create a constructor
	public testSumDoublePrimitive(int counter, int ARRAYSIZE) {
		this.counter = counter;
		this.ARRAY_SIZE = ARRAYSIZE;
	}

	//a run method
	public void run() {
		double[] values = new double[ARRAY_SIZE];
		for (int k = 0; k < ARRAY_SIZE; k++)
			values[k] = k + 1;

		sum = 0.0;
		for (int count = 0, i = 0; count < counter; count++, i++) {
			if (i >= ARRAY_SIZE)
				i = 0;
			sum = sum + values[i];
		}
	}

	//print a toString
	public String toString() {
		return String.format("Sum array of double primitives with count=%,d\n",
				counter);
	}

	//print a toStringFinal.
	public String toStringFinal() {
		return "sum = " + sum + "\n";
	}

	//print a toStringElapsed.
	public String toStringElapsed(StopWatch timer) {
		return String.format("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}

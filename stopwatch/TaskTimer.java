package stopwatch;
/**
 * A task timer class.
 * @author Narut Poovorakit
 * @version 31.01.2015
 *
 */
public class TaskTimer {
	//create 2 object.A stopwatch object and speedtest object.
	static StopWatch timer = new StopWatch();
	SpeedTest speedTest = new SpeedTest();
	
	/**
	 * This method is command to measure and print out all of toString.
	 * @param runnable is a class of each task.
	 */
	public static void measureAndPrint(Runnable runnable){
		//Start the stopwatch.
		timer.start();
		//Run the task.
		runnable.run();
		//Stop the stopwatch.
		timer.stop();
		//Print a toString method of each task.
		System.out.print(runnable.toString());
		//Print a toStringFinal method of each task.
		System.out.print(runnable.toStringFinal());
		//Print a toStringElapsed method of each task.
		System.out.print(runnable.toStringElapsed(timer));
	}
}

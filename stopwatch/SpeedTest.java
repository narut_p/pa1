package stopwatch;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;
/**
 * The speedtest class is the main class.
 * @author Net
 * @version 31.01.2015
 */
public class SpeedTest {
	// the loop counter used in the tasks (can be changed)
	private int counter = 100000;
	// size of the array used in floating point tasks.
	// Don't make this too big to avoid slowing down test with paging
	// or possible out-of-memory error.
	static final int ARRAY_SIZE = 500000;
	
	/**
	 * Set the loop count used in tests.
	 * @param count is number of loop iterations
	 */
	public void setCounter(int count) { this.counter = count; }
	
	//get counter.
	public int getCounter() {
		return counter;
	}
	
	//get the size of an array.
	public int getArraySize() {
		return ARRAY_SIZE;
	}
	/**
	 * Run the tasks.
	 */
	public static void main(String[] args) throws IOException {
		
		//Create a task timer object.
		TaskTimer taskTimer = new TaskTimer();
		
		//Create a speedtest object.
		SpeedTest speedTest = new SpeedTest();
		
		//Class1//
		//send counter to the testAppendToString class.
		testAppendToString AppendToString= new testAppendToString(speedTest.getCounter());
		taskTimer.measureAndPrint(AppendToString);
		
		//Class2//
		//send counter to the testAppendToStringBuilder class.
		testAppendToStringBuilder AppendBuilder = new testAppendToStringBuilder(speedTest.getCounter());
		taskTimer.measureAndPrint(AppendBuilder);
		
		//set counter to one hundred million.
		speedTest.setCounter(100000000); 
		
		//Class3//
		//send counter to the testSumDoublePrimitive class.
		//send the size of an array.
		testSumDoublePrimitive AppendPrimitive = new testSumDoublePrimitive(speedTest.getCounter(), speedTest.getArraySize());
		taskTimer.measureAndPrint(AppendPrimitive);
		
		//Class4//
		//send counter to the testSumDouble class.
		//send the size of an array.
		testSumDouble AppenDouble = new testSumDouble(speedTest.getCounter(), speedTest.getArraySize());
		taskTimer.measureAndPrint(AppenDouble);
		
		//Class5//
		//send counter to the testSumBigDecimal class.
		//send the size of an array.
		testSumBigDecimal AppenDecimal = new testSumBigDecimal(speedTest.getCounter(), speedTest.getArraySize());
		taskTimer.measureAndPrint(AppenDecimal);
		
		
	}
}
package stopwatch;

import java.math.BigDecimal;
/**
 * A testSumBigDecimal class.
 * @author Narut Poovorakit
 * @version 31.01.2015
 *
 */
public class testSumBigDecimal implements Runnable {
	//create an instance variables.
	private int counter;
	private int ARRAY_SIZE;
	private BigDecimal sum;

	/**
	 * initiate a constructor.
	 * @param counter that count from speedtest.
	 * @param ARRAYSIZE is a size of array.
	 */
	public testSumBigDecimal(int counter, int ARRAYSIZE) {
		this.counter = counter;
		this.ARRAY_SIZE = ARRAYSIZE;
	}

	//method that run.
	public void run() {
		BigDecimal[] values = new BigDecimal[ARRAY_SIZE];
		for (int i = 0; i < ARRAY_SIZE; i++)
			values[i] = new BigDecimal(i + 1);
		sum = new BigDecimal(0.0);
		for (int count = 0, i = 0; count < counter; count++, i++) {
			if (i >= ARRAY_SIZE)
				i = 0;
			sum = sum.add(values[i]);
		}
	}

	//print toString.
	public String toString() {
		return String.format("Sum array of BigDecimal with count=%,d\n",
				counter);
	}

	//print toStringFinal.
	public String toStringFinal() {
		return "sum = " + sum + "\n";
	}

	//print toStringElapsed.
	public String toStringElapsed(StopWatch timer) {
		return String.format("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}
